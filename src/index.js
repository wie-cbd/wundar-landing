$(document).ready(function () {

});


var lastScrollTop = 0;
var mobileScrollTop = 0;



function toggleNav() {
    this.toggleClass('fullnav', 'fullnav-active');
}

function toggleClass(id, classname) {
    document.getElementById(id).classList.toggle(classname);
}

$(window).scroll(function (event) {
    var st = $(this).scrollTop();
    toggleNavbarScroll(st, lastScrollTop, 250);
    lastScrollTop = st;
});

function toggleNavbarScroll(st, comparator, maxScroll) {
    if (comparator < maxScroll) { 
    } else if (st > comparator && st > maxScroll) { 
    } else { 
    }
}

function scrollToID(id) {
    document.querySelector(id).scrollIntoView({
        behavior: "smooth",
        block: "start",
        inline: "nearest"
    });
    document.getElementById('mainNav').classList.remove('navbar-active');
    document.getElementById('mainNav').classList.add('navbar-inactive');
}

function fullNavLink(id) {
    toggleNav();
    scrollToID(id);
}